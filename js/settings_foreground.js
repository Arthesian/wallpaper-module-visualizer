var foregroundSettingsObject = {};
var foreground_settings = function(settings) {
    // Select foreground type
    var setforeground = function() {
        
        WALLPAPER.foreground.setImage(foregroundSettingsObject.image);
    }

    // Enable/Disable foreground
    if (settings.foreground_enable) {
        if (!settings.foreground_enable.value) {
            WALLPAPER.foreground.Enable(false);
        } else {
            WALLPAPER.foreground.Enable(true);
        }
        setforeground();
    }

    // Select foreground image
    if (settings.foreground_image) {
        if (settings.foreground_image.value !== "") {
            foregroundSettingsObject.image = "file:///" + settings.foreground_image.value;
        } else {
            foregroundSettingsObject.image = null;
        }
        setforeground();
    }

    if(settings.foreground_image_size) {
        WALLPAPER.foreground.settings.image_max_height = settings.foreground_image_size.value || 0; // 0 = disabled
    }

    if(settings.foreground_offset_x) {
        WALLPAPER.foreground.settings.image_offset_x = settings.foreground_offset_x.value || 0;
    }
    
    if(settings.foreground_offset_y) {
        WALLPAPER.foreground.settings.image_offset_y = settings.foreground_offset_y.value || 0;
    }

    // Z-index
    if(settings.foreground_image_zindex) {
        WALLPAPER.foreground.settings.zIndex = settings.foreground_image_zindex.value;
    }
}